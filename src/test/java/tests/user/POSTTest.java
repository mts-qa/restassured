package tests.user;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;


public class POSTTest {

    protected HashMap<String, String> mapUsers;
    @Before
    public void setUp () {
        RestAssured.baseURI = "https://reqres.in/api";
        mapUsers = new HashMap<>();
        mapUsers.put("email","qa@reqres.in");
        mapUsers.put("password","qa2024");
    }

    @Test
    public void postCreateUser() {
        Response response = RestAssured
                .given()
                .contentType("application/json")
                .body(mapUsers)
                .when()
                .post("/users")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals(201, response.statusCode());
    }

    @Test
    public void postRegUnsuccessful() {
        Response response = RestAssured
                .given()
                .contentType("application/json")
                .body(mapUsers)
                .when()
                .post("/register")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals(400, response.statusCode());
    }
}