package tests.user;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;

public class DELETETest {

    protected HashMap<String, String> mapUsers;
    @Before
    public void setUp () {
        RestAssured.baseURI = "https://reqres.in/api";
        mapUsers = new HashMap<>();
        mapUsers.put("email","qa@reqres.in");
        mapUsers.put("password","qa2024");
    }

    @Test
    public void deleteExistingUser() {
        Response response = RestAssured
                .when()
                .delete("/users/2")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals(204, response.statusCode());
    }

    @Test
    public void deleteNonExistingUser() {
        Response response = RestAssured
                .when()
                .delete("/users/000")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals(204, response.statusCode());
    }
}