package tests.user;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import java.util.HashMap;
import java.util.Map;

public class PUTTest {
    @Before
    public void setUp () {
        RestAssured.baseURI = "https://reqres.in/api";
    }

    @Test
    public void putUser$() {
        Response response = RestAssured
                .given()
                .contentType("application/json")
                .body(Map.of("$", "#"))
                .when()
                .put("/users/$")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals(200, response.statusCode());
    }

    @Test
    public void putUserAibek() {
        HashMap<String, String> mapUsers = new HashMap<>();
        mapUsers.put("first_name","Aibek");
        mapUsers.put("last_name","Akhmetkazy");
        Response response = RestAssured
                .given()
                .contentType("application/json")
                .body(mapUsers)
                .when()
                .put("/users/1")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals("Aibek", response.jsonPath().getString("first_name"));
    }
}