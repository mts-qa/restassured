package tests.user;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class GETTest {
    @Before
    public void setUp () {
        RestAssured.baseURI = "https://reqres.in/api";
    }

    @Test
    public void getUser1() {
        Response response = RestAssured
                .when()
                .get("https://reqres.in/api/users/1")
                .then()
                .extract()
                .response();
        response.prettyPrint();
        Assertions.assertEquals(200, response.statusCode());
    }

    @Test
    public void getUser200() {
        Response response = RestAssured
                .when()
                .get("https://reqres.in/api/users/200")
                .then()
                .extract()
                .response();
        Assertions.assertEquals(404, response.statusCode());
    }

}

